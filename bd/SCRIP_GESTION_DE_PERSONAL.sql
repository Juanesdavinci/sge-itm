-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 20, 2017 at 04:49 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+05:00";



--
-- Database: `bdmanejopersonaltecnico`
--

-- --------------------------------------------------------

--
-- Table structure for table `TBLCARGOEMPLEADO`
--

CREATE TABLE `TBLCARGOEMPLEADO` (
  `LNGIDCARGO` int(11) NOT NULL DEFAULT '0',
  `STRNOMBRECARGO` varchar(100) DEFAULT NULL,
  `STRDESCRIPCION_CARGO` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `TBLCLIENTE`
--

CREATE TABLE `TBLCLIENTE` (
  `LNGIDCLIENTE` int(11) NOT NULL DEFAULT '0',
  `STRNOMBRECLIENTE` varchar(100) DEFAULT NULL,
  `STRDIRECCION` varchar(100) DEFAULT NULL,
  `STRTELEFONO` varchar(100) DEFAULT NULL,
  `LNGIDZONA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `TBLEMPLEADO`
--

CREATE TABLE `TBLEMPLEADO` (
  `LNGIDEMPLEADO` int(11) NOT NULL DEFAULT '0',
  `STRIDENTIFICACION` varchar(20) DEFAULT NULL,
  `STRNOMBREEMPLEADO` varchar(100) DEFAULT NULL,
  `STRDIRECCIONEMPLEADO` varchar(100) DEFAULT NULL,
  `STREMAIL` varchar(100) DEFAULT NULL,
  `STRCELULAR` varchar(100) DEFAULT NULL,
  `STRTELEFONO` varchar(100) DEFAULT NULL,
  `DTMFECHAINGRESO` date DEFAULT NULL,
  `LNGIDCARGO` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TBLEMPLEADO`
--

INSERT INTO `TBLEMPLEADO` (`LNGIDEMPLEADO`, `STRIDENTIFICACION`, `STRNOMBREEMPLEADO`, `STRDIRECCIONEMPLEADO`, `STREMAIL`, `STRCELULAR`, `STRTELEFONO`, `DTMFECHAINGRESO`, `LNGIDCARGO`) VALUES
(1, 'Laura', 'Bruce Murray', 'Ap #321-572 Quis Avenue', 'Aliquam.erat.volutpat@eleifend.net', '(459) 212-4796', '4-398-297-3514', '0101-04-04', 1),
(2, 'Arsenio', 'Lavinia Hart', '3430 Iaculis Street', 'et@libero.edu', '(378) 977-2361', '4-459-618-0360', '2828-10-10', 2),
(3, 'Neville', 'Ethan Foster', 'Ap #577-8075 Praesent Rd.', 'eu.odio.tristique@nec.co.uk', '(361) 689-1338', '4-458-826-0464', '0303-11-11', 1),
(4, 'Dean', 'Allistair Knight', 'Ap #316-9699 Vel St.', 'dictum@vulputate.com', '(502) 817-9139', '4-465-912-9606', '2020-08-08', 2),
(5, 'Madeson', 'Sigourney Vance', '279-6638 Fusce St.', 'montes.nascetur.ridiculus@In.co.uk', '(114) 208-6410', '4-496-214-3273', '0202-07-07', 1),
(6, 'Keely', 'Aileen Burns', '610-6348 Pede, Av.', 'pede.Cum.sociis@semper.org', '(578) 135-1289', '4-761-760-2923', '2020-06-06', 1),
(7, 'Renee', 'Tashya Ratliff', 'P.O. Box 208, 1746 Cras Road', 'tincidunt.Donec@vitae.org', '(995) 203-7273', '4-754-415-6814', '2424-10-10', 1),
(8, 'Shay', 'Chantale Ortega', 'Ap #111-4375 Sem Avenue', 'tempor@dolordapibusgravida.co.uk', '(286) 946-8868', '4-103-925-3261', '0808-09-09', 2),
(9, 'Audrey', 'Marcia Wade', '741-1922 Maecenas Street', 'sed.sem@magnaPhasellus.org', '(965) 589-2155', '4-322-364-9588', '2828-02-02', 2),
(10, 'Seth', 'Derek Hayes', 'P.O. Box 579, 7004 Aliquam Av.', 'aliquet.metus@etultricesposuere.edu', '(324) 451-1603', '4-221-743-8639', '0505-04-04', 2),
(11, 'Aline', 'Nehru Dawson', 'Ap #965-1789 Eget Rd.', 'tellus.Nunc@odioa.ca', '(521) 892-4430', '4-669-114-9899', '2323-06-06', 1),
(12, 'Amber', 'Lila Rogers', '7785 Auctor St.', 'pharetra.sed@pretiumnequeMorbi.net', '(269) 960-1224', '4-598-925-5124', '2525-04-04', 2),
(13, 'Moana', 'Dorothy Tucker', 'P.O. Box 240, 4172 In Rd.', 'ipsum.dolor.sit@massa.ca', '(699) 485-2431', '4-332-396-5900', '2828-04-04', 2),
(14, 'Jack', 'Bianca Garner', '6609 Interdum Avenue', 'parturient@variuseteuismod.co.uk', '(834) 629-9529', '4-610-919-2553', '2929-01-01', 2),
(15, 'Petra', 'Colin Marks', 'P.O. Box 438, 2866 Nisl. Rd.', 'Sed.molestie@odio.edu', '(184) 271-7009', '4-871-374-2952', '1717-08-08', 1),
(16, 'Barbara', 'Chandler Strong', '4661 Porttitor St.', 'scelerisque@urnaet.com', '(712) 391-9381', '4-119-477-0852', '2323-02-02', 2),
(17, 'Roary', 'Rylee Rush', '389-1657 Semper St.', 'erat@etmalesuadafames.net', '(828) 216-3236', '4-216-139-3476', '2525-07-07', 2),
(18, 'Daquan', 'Athena King', 'Ap #973-3162 Laoreet Rd.', 'iaculis.aliquet@arcu.edu', '(610) 590-7918', '4-195-351-0654', '2323-12-12', 2),
(19, 'Brynne', 'Ursa Nieves', 'P.O. Box 813, 7770 Morbi Ave', 'blandit.congue.In@nonegestas.ca', '(969) 723-1618', '4-352-199-2939', '2020-05-05', 2),
(20, 'Xandra', 'Alyssa Collins', '7517 Odio, St.', 'mollis.nec.cursus@gravida.org', '(731) 110-0243', '4-888-306-1810', '1515-08-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `TBLREPORTE`
--

CREATE TABLE `TBLREPORTE` (
  `LNGIDREPORTE` mediumint(9) DEFAULT NULL,
  `LNGIDEMPLEADO` varchar(255) DEFAULT NULL,
  `LNGIDCLIENTE` mediumint(9) DEFAULT NULL,
  `LNGESTADO` mediumint(9) DEFAULT NULL,
  `STRDIRECCIONREPORTE` varchar(255) DEFAULT NULL,
  `LNGIDZONA` mediumint(9) DEFAULT NULL,
  `STRMATERIALES` text,
  `DTMFECHAREPORTE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TBLREPORTE`
--

INSERT INTO `TBLREPORTE` (`LNGIDREPORTE`, `LNGIDEMPLEADO`, `LNGIDCLIENTE`, `LNGESTADO`, `STRDIRECCIONREPORTE`, `LNGIDZONA`, `STRMATERIALES`, `DTMFECHAREPORTE`) VALUES
(1, 'Casey B. English', 2, 3, 'P.O. Box 575, 5329 Mauris St.', 1, 'Lorem', '090912122017201720172017'),
(2, 'Kylee Adams', 3, 3, '358-5758 Nam St.', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '101011112018201820182018'),
(3, 'Randall Q. Cote', 2, 2, 'Ap #185-6403 Vehicula Rd.', 7, 'Lorem ipsum dolor', '111104042017201720172017'),
(4, 'Lewis G. Burke', 1, 4, '9521 Elit Rd.', 8, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '070707072017201720172017'),
(5, 'Isabella Warner', 4, 3, '2213 Malesuada Road', 8, 'Lorem ipsum dolor sit amet,', '161606062017201720172017'),
(6, 'Craig Owens', 1, 3, '998-9229 Nisi Rd.', 8, 'Lorem ipsum', '252505052018201820182018'),
(7, 'Tyrone Snider', 3, 4, 'Ap #832-7013 Luctus Road', 8, 'Lorem ipsum dolor sit amet, consectetuer', '151502022017201720172017'),
(8, 'Erin U. Grimes', 3, 2, '4224 Nam St.', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '232308082018201820182018'),
(9, 'Andrew H. Cochran', 8, 3, '736-8633 Ut St.', 10, 'Lorem', '232312122017201720172017'),
(10, 'Marsden C. Leon', 1, 2, '7869 Aliquam Av.', 3, 'Lorem ipsum', '232312122016201620162016'),
(11, 'Allistair Z. Avery', 3, 2, 'P.O. Box 894, 9620 Proin Av.', 8, 'Lorem ipsum', '252506062018201820182018'),
(12, 'Imelda J. Osborne', 5, 2, 'Ap #690-7747 Nec Av.', 2, 'Lorem ipsum dolor sit amet, consectetuer', '222211112016201620162016'),
(13, 'Vance F. Baird', 1, 1, 'Ap #212-3638 Euismod Street', 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '171712122017201720172017'),
(14, 'Akeem Peterson', 3, 4, '1952 Nunc Rd.', 4, 'Lorem', '272708082017201720172017'),
(15, 'Madonna U. Hawkins', 8, 1, '4865 A, Avenue', 9, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '080812122016201620162016'),
(16, 'Autumn L. Craig', 4, 1, '2757 Ut Street', 9, 'Lorem ipsum dolor sit', '141405052017201720172017'),
(17, 'Phelan W. Trujillo', 5, 4, '242-6907 Dolor St.', 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '010106062017201720172017'),
(18, 'Charissa N. Carpenter', 8, 4, '359-2463 Orci Rd.', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '181804042017201720172017'),
(19, 'Unity Torres', 10, 3, 'P.O. Box 349, 5630 Rutrum Rd.', 10, 'Lorem ipsum dolor sit amet,', '212104042017201720172017'),
(20, 'Kevin Mclean', 5, 3, 'Ap #927-5434 Dolor Road', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '030307072017201720172017'),
(21, 'Malcolm Harding', 4, 2, 'P.O. Box 396, 2256 Sem Rd.', 4, 'Lorem ipsum dolor', '171708082018201820182018'),
(22, 'Robin Burgess', 7, 3, '623-6367 Dui Road', 9, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '191902022017201720172017'),
(23, 'Elizabeth N. Houston', 3, 3, '883-274 Luctus Av.', 7, 'Lorem ipsum', '141405052017201720172017'),
(24, 'Naida Barry', 7, 2, 'Ap #514-3859 Pellentesque Rd.', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '080804042018201820182018'),
(25, 'Kelly Kirkland', 5, 1, '958-9134 Id, St.', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '050509092017201720172017'),
(26, 'Judah G. Diaz', 6, 4, '3751 Molestie Ave', 8, 'Lorem', '080810102018201820182018'),
(27, 'Amery H. Silva', 4, 3, '100-7956 Donec Ave', 2, 'Lorem ipsum dolor sit amet,', '090906062018201820182018'),
(28, 'Rachel Hensley', 6, 4, 'Ap #265-6441 Libero Av.', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '202012122016201620162016'),
(29, 'Tad Whitaker', 3, 1, '533-5655 Magna Ave', 5, 'Lorem ipsum', '232304042018201820182018'),
(30, 'Beck S. Doyle', 4, 3, '5265 Elit, St.', 2, 'Lorem ipsum dolor sit amet, consectetuer', '111103032017201720172017'),
(31, 'Benjamin D. Osborn', 4, 3, '708-6383 Tellus. Rd.', 7, 'Lorem ipsum dolor sit amet,', '101009092017201720172017'),
(32, 'Giacomo U. Holt', 5, 3, '5263 Augue, St.', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '242401012017201720172017'),
(33, 'Jennifer Garza', 8, 3, '6544 Maecenas Av.', 8, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '181811112018201820182018'),
(34, 'Sigourney K. Fischer', 6, 4, '250 Aliquam Avenue', 2, 'Lorem ipsum dolor sit amet,', '262607072018201820182018'),
(35, 'Arsenio Hall', 2, 4, 'Ap #390-647 Dui, Rd.', 10, 'Lorem ipsum', '101011112018201820182018'),
(36, 'Petra Q. Bonner', 4, 1, 'P.O. Box 717, 1449 Mauris Ave', 4, 'Lorem ipsum dolor', '232301012017201720172017'),
(37, 'Virginia Farmer', 9, 1, '7186 Consectetuer Av.', 10, 'Lorem ipsum dolor', '161608082018201820182018'),
(38, 'Zelda T. Summers', 3, 2, 'P.O. Box 806, 9528 Lorem St.', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '191906062018201820182018'),
(39, 'Moana U. Stone', 6, 3, '3591 Velit Rd.', 6, 'Lorem ipsum dolor', '161603032017201720172017'),
(40, 'Minerva G. Holland', 10, 2, 'P.O. Box 291, 9974 Ligula. St.', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '020204042017201720172017'),
(41, 'Kibo B. Norton', 10, 3, '149-5026 Ultrices, St.', 3, 'Lorem ipsum dolor', '040407072018201820182018'),
(42, 'Inga Case', 1, 1, '872 Vulputate St.', 10, 'Lorem ipsum dolor sit amet,', '191904042017201720172017'),
(43, 'Dennis V. Hart', 3, 1, 'P.O. Box 772, 3955 Sodales. Avenue', 9, 'Lorem ipsum dolor sit amet,', '080807072017201720172017'),
(44, 'Beverly Boyer', 10, 2, '5796 Lacinia Street', 6, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '191907072017201720172017'),
(45, 'Sylvester S. Levine', 1, 2, 'Ap #286-2907 Aliquet Rd.', 4, 'Lorem ipsum dolor', '171704042017201720172017'),
(46, 'Galena Fulton', 7, 1, 'P.O. Box 795, 9817 Luctus St.', 6, 'Lorem ipsum dolor sit amet, consectetuer', '151505052018201820182018'),
(47, 'Farrah Q. Sweet', 7, 4, '8836 Eget St.', 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '141405052017201720172017'),
(48, 'Brennan Galloway', 3, 1, '8910 Donec Road', 8, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '151512122016201620162016'),
(49, 'Shelley Christensen', 2, 4, '945-1056 Dignissim St.', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '191904042018201820182018'),
(50, 'Breanna W. Cox', 3, 4, '9186 Ligula. Rd.', 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '141408082017201720172017'),
(51, 'Olivia Q. Horn', 10, 4, '866-5479 Primis Rd.', 10, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '262612122016201620162016'),
(52, 'Dominic Pitts', 6, 4, 'P.O. Box 478, 4073 Integer Avenue', 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '252504042018201820182018'),
(53, 'Emmanuel Holland', 4, 3, '5427 Et Road', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '242411112017201720172017'),
(54, 'Leandra Hurley', 1, 2, '1122 Nunc Avenue', 5, 'Lorem ipsum dolor sit amet,', '222204042017201720172017'),
(55, 'Mari Miranda', 7, 2, '8703 Sagittis. St.', 1, 'Lorem', '232301012017201720172017'),
(56, 'Vance H. Freeman', 6, 4, 'Ap #968-187 Blandit St.', 6, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '020212122017201720172017'),
(57, 'Kristen X. Church', 9, 4, '1354 Metus St.', 2, 'Lorem', '272712122016201620162016'),
(58, 'Raphael Castro', 1, 2, 'Ap #915-194 Lorem, Ave', 9, 'Lorem ipsum dolor sit amet,', '262607072018201820182018'),
(59, 'Wendy T. Reed', 5, 2, '4924 Magna Rd.', 7, 'Lorem', '171712122017201720172017'),
(60, 'Audrey Erickson', 7, 3, 'Ap #796-6523 Rhoncus. St.', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '121205052018201820182018'),
(61, 'Vaughan A. Flynn', 4, 4, '339-3437 Dapibus Rd.', 8, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '040410102018201820182018'),
(62, 'Eric Petty', 1, 2, 'P.O. Box 369, 3996 Et Av.', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '191901012017201720172017'),
(63, 'Eaton Suarez', 2, 2, 'P.O. Box 274, 5530 Sem, St.', 4, 'Lorem ipsum dolor', '060612122017201720172017'),
(64, 'Aidan Valentine', 10, 4, '3121 Donec St.', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '202005052017201720172017'),
(65, 'Levi Hess', 1, 1, '3518 Hendrerit Avenue', 6, 'Lorem ipsum dolor sit amet, consectetuer', '242408082017201720172017'),
(66, 'Avye X. Finley', 8, 3, 'Ap #347-3281 Diam. St.', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '090911112018201820182018'),
(67, 'Joseph Marks', 5, 2, 'Ap #810-1204 Libero. St.', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '212104042017201720172017'),
(68, 'Hoyt Osborne', 9, 2, '3846 Mauris. Rd.', 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '080801012017201720172017'),
(69, 'Alan G. Watts', 10, 2, '582-8938 Facilisis. Rd.', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '050512122016201620162016'),
(70, 'Nyssa H. Duke', 10, 3, '5373 Parturient Rd.', 10, 'Lorem', '313112122016201620162016'),
(71, 'Alexandra Underwood', 5, 1, 'P.O. Box 669, 3677 Velit Avenue', 10, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '020212122016201620162016'),
(72, 'Phillip Sanders', 5, 3, 'Ap #349-5394 Nunc, Street', 8, 'Lorem ipsum dolor sit amet,', '030312122016201620162016'),
(73, 'Lois J. Sullivan', 8, 2, 'P.O. Box 637, 9659 Sed Road', 9, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '030306062018201820182018'),
(74, 'Gareth Q. Travis', 2, 3, '656-973 Eu Road', 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '131310102017201720172017'),
(75, 'Kelly Z. Spears', 6, 3, '653-2389 Facilisis Avenue', 2, 'Lorem ipsum dolor sit amet,', '161605052017201720172017'),
(76, 'Forrest William', 4, 2, '5933 Velit. Road', 8, 'Lorem ipsum', '303012122016201620162016'),
(77, 'Sonia P. Barry', 10, 3, 'P.O. Box 733, 5575 Ullamcorper Rd.', 1, 'Lorem ipsum', '040409092017201720172017'),
(78, 'Indigo Mcclure', 3, 3, '839-9789 Eu Rd.', 6, 'Lorem', '272708082017201720172017'),
(79, 'Amal G. Prince', 1, 3, '420-6401 Scelerisque St.', 10, 'Lorem', '161605052018201820182018'),
(80, 'Fatima Serrano', 6, 3, '9446 Interdum. Street', 8, 'Lorem ipsum', '040411112017201720172017'),
(81, 'Marsden Blevins', 8, 1, 'P.O. Box 957, 3413 Egestas St.', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '020210102017201720172017'),
(82, 'Griffith M. Hays', 9, 3, '697-2521 Primis Rd.', 10, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '090910102017201720172017'),
(83, 'Olga Banks', 9, 2, '2604 Lorem Ave', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '303011112016201620162016'),
(84, 'Dalton U. Colon', 8, 2, '1307 Scelerisque Avenue', 6, 'Lorem', '282802022018201820182018'),
(85, 'Mannix Glenn', 2, 1, '999-5843 Fringilla. Road', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', '080810102018201820182018'),
(86, 'Wyatt Stanton', 2, 1, 'P.O. Box 301, 4202 Ornare, Rd.', 6, 'Lorem ipsum dolor sit', '232307072017201720172017'),
(87, 'Elizabeth Buckner', 4, 3, '3861 Tellus Ave', 6, 'Lorem ipsum dolor', '040407072018201820182018'),
(88, 'Bree B. Herman', 2, 1, 'P.O. Box 948, 280 Pede, Road', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '060612122017201720172017'),
(89, 'Price Larsen', 3, 4, 'P.O. Box 281, 6562 Molestie Av.', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '232302022017201720172017'),
(90, 'Kellie Leonard', 6, 4, 'Ap #179-5674 Gravida Ave', 8, 'Lorem ipsum dolor sit amet, consectetuer', '070711112018201820182018'),
(91, 'Arden Finley', 3, 1, 'Ap #664-8145 A Road', 5, 'Lorem ipsum dolor sit', '101004042018201820182018'),
(92, 'Patrick V. Garrett', 8, 1, '5356 Varius Rd.', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', '272704042018201820182018'),
(93, 'Scarlett B. Sullivan', 7, 4, 'P.O. Box 129, 7079 Enim Rd.', 2, 'Lorem', '161603032018201820182018'),
(94, 'Lillith Mayo', 7, 3, 'P.O. Box 250, 1574 Magna Ave', 10, 'Lorem ipsum', '030310102018201820182018'),
(95, 'Alexis Valdez', 4, 2, 'P.O. Box 832, 7166 Nec, Avenue', 9, 'Lorem ipsum dolor sit', '212109092017201720172017'),
(96, 'Gavin P. Sparks', 2, 4, 'Ap #112-9667 Et Street', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '101006062017201720172017'),
(97, 'Allen O. Acevedo', 6, 4, 'P.O. Box 528, 8548 Eu Rd.', 3, 'Lorem ipsum', '222211112016201620162016'),
(98, 'Sonya S. Mayo', 9, 3, '431-5058 Arcu. Av.', 6, 'Lorem ipsum dolor sit', '010109092017201720172017'),
(99, 'Orlando Horton', 2, 4, '1743 Magna. Rd.', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', '070706062017201720172017'),
(100, 'Ruth Velez', 6, 4, 'Ap #491-376 Vel Avenue', 3, 'Lorem ipsum dolor', '292912122016201620162016');

-- --------------------------------------------------------

--
-- Table structure for table `TBLTIPOUSUARIO`
--

CREATE TABLE `TBLTIPOUSUARIO` (
  `LNGIDTIPOUSUARIO` int(11) NOT NULL,
  `STRTIPOUSUARIO` varchar(20) DEFAULT NULL,
  `STRDESCRIPCION_TIPOUSUARIO` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `TBLUSUARIO`
--

CREATE TABLE `TBLUSUARIO` (
  `LNGIDUSUARIO` int(11) NOT NULL DEFAULT '0',
  `LNGIDEMPLEADO` int(11) DEFAULT NULL,
  `STRUSUARIO` varchar(500) DEFAULT NULL,
  `STRCONTRASENA` varchar(100) DEFAULT NULL,
  `BLNESTADO` varchar(1) DEFAULT NULL,
  `LNGIDTIPOUSUARIO` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TBLUSUARIO`
--

INSERT INTO `TBLUSUARIO` (`LNGIDUSUARIO`, `LNGIDEMPLEADO`, `STRUSUARIO`, `STRCONTRASENA`, `BLNESTADO`, `LNGIDTIPOUSUARIO`) VALUES
(0, 1, 'Juan', '1234', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `TBLZONA`
--

CREATE TABLE `TBLZONA` (
  `LNGIDZONA` int(11) NOT NULL DEFAULT '0',
  `STRZONA` varchar(100) DEFAULT NULL,
  `STRDESCRIPCIONZONA` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `TBLCARGOEMPLEADO`
--
ALTER TABLE `TBLCARGOEMPLEADO`
  ADD PRIMARY KEY (`LNGIDCARGO`);

--
-- Indexes for table `TBLCLIENTE`
--
ALTER TABLE `TBLCLIENTE`
  ADD PRIMARY KEY (`LNGIDCLIENTE`);

--
-- Indexes for table `TBLEMPLEADO`
--
ALTER TABLE `TBLEMPLEADO`
  ADD PRIMARY KEY (`LNGIDEMPLEADO`);

--
-- Indexes for table `TBLTIPOUSUARIO`
--
ALTER TABLE `TBLTIPOUSUARIO`
  ADD PRIMARY KEY (`LNGIDTIPOUSUARIO`);

--
-- Indexes for table `TBLUSUARIO`
--
ALTER TABLE `TBLUSUARIO`
  ADD PRIMARY KEY (`LNGIDUSUARIO`);

--
-- Indexes for table `TBLZONA`
--
ALTER TABLE `TBLZONA`
  ADD PRIMARY KEY (`LNGIDZONA`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `TBLTIPOUSUARIO`
--
ALTER TABLE `TBLTIPOUSUARIO`
  MODIFY `LNGIDTIPOUSUARIO` int(11) NOT NULL AUTO_INCREMENT;