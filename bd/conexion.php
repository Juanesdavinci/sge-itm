<?php


class Conexion 

{
	
   public  $mysqli;
   public  $mysqli_estado;


	function __construct()
	{
	  	}

	public function conectar(){
		
    //	global $mysqli, $mysqli_estado; 
	require_once ('datos_conexion.php');
	$this->mysqli = new mysqli($hostname, $username,$password, $database); 
	//en PHP para inicializar un objeto se utiliza el método new; luego, para acceder a cada uno de sus métodos se utiliza el operador ->
	//La clase mysqli, representa una conexión entre PHP y una base de datos MySQL.

	if ($this->mysqli->connect_errno) {	//mysqli_connect_errno Devuelve el código de error de la última llamada.
		//die( "Falló la conexión a MySQL: (" . $mysqli -> mysqli_connect_errno() 
		//. ") " . $mysqli -> mysqli_connect_error());//mysqli_connect_error Devuelve la descripción del error de la última llamada.
		$this->mysqli_estado = "Falló la conexión a MySQL: (" . $this->mysqli -> mysqli_connect_errno() . ") " . $this->mysqli -> mysqli_connect_error();//mysqli_connect_error Devuelve la descripción del error de la última llamada.
	}else{
		$this->mysqli_estado = "Conexión exitosa con la BD $database <br>";
	}

	//return $mysqli;S
	return $this->mysqli_estado;
	//echo $mysqli_estado;	
	//Cierre de conexion 
	//$mysqli -> mysqli_close();
}

}



	
?>
