<!DOCTYPE html>
<html lang="es">

 
    
      
    <head>
        <meta charset="utf-8"/>
        <title>SICA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico"/>
        <link rel="stylesheet" href="css/estilos.css"/>
        <link rel="stylesheet" href="estilos_.css">
        <link rel="stylesheet" href="css.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
 
 
    </head>
    <body>

        

        <header id="menu">
            <header id="logo">
                <a href="index.php">
                    <img src="img/logo.png"/>
                </a>
            </header>
            <ul class="menu">
                <li><a href="#"><i class="icono izquierda fa fa-cogs" aria-hidden="true"></i>Gestionar<i class="icono derecha fa fa-chevron-down" aria-hidden="true"></i></a>
                    <ul class="submenu">
                        <li><a href="#">Salon<i class="icono derecha fa fa-chevron-down" aria-hidden="true"></i></a>
                            <ul>
                                <li><a href="#">Nuevo</a></li>
                                <li><a href="#">Editar</a></li>
                                <li><a href="#">Eliminar</a></li>
                                <li><a href="#">Consultar</a></li>
                            </ul>
                        </li>
                        <li><a href="#">item 3</a></li>
                        <li><a href="#">item 4</a></li>
                        <li><a href="#">Grupo<i class="icono derecha fa fa-chevron-down" aria-hidden="true"></i></a>
                            <ul>
                                <li><a href="#">Nuevo</a></li>
                                <li><a href="#">Editar</a></li>
                                <li><a href="#">Eliminar</a></li>
                                <li><a href="#">Consultar</a></li>
                            </ul>
                        </li>
                        <li><a href="#">item 5</a></li>
                        <li><a href="#">item 6</a></li>
                        <li><a href="#">item 7</a></li>
                        <li><a href="#">item 8</a></li>
                        <li><a href="#">item 9</a></li>
                        <li><a href="#">item 10 largo </a></li>
                    </ul>
                </li>
            </ul>
        </header>
 
 
    </body>
</html>