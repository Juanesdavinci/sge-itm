<!DOCTYPE html>
<html>
<body>

<p>This example uses the addEventListener() method to attach a "blur" event to an input element.</p>

<p>Write something in the input field, and then click outside the field to lose focus (blur).</p>

<input type="text" id="fname">

<script>
document.getElementById("fname").addEventListener("blur", myFunction(this));

function myFunction() {
    alert("holla digito el usuario ");
}
</script>

</body>
</html>