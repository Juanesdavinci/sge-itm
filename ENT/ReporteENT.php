<?php
include ('../bd/conexion.php');



class ReporteENT{

	private $ReporteAsesor;
	public $db;

	public function __construct()
	{  
		$objConexion= new Conexion();
		$objConexion->conectar();
        $this->ReporteAsesor=array();
        $this->ReporteTecnico=array();
        $this->DetalleReporter=array();
		$this->db=$objConexion->mysqli;
	}

	public function get_ReporteAsesor()
	{  
    	$query = $this->db-> query ("SELECT * FROM tblreporte");
        while ($valores = mysqli_fetch_array($query)) {
    		$this->ReporteAsesor[]=$valores; 
    	}
		return $this->ReporteAsesor;
	} 


    public function get_ReporteTecnico()
	{  
    	$query = $this->db-> query ("SELECT * FROM tblreporte");
        while ($valores = mysqli_fetch_array($query)) {
    		$this->ReporteTecnico[]=$valores; 
    	}
		return $this->ReporteTecnico;
	} 
    
    public function get_DetalleReporte($id)
	{  
    	$query = $this->db-> query ("SELECT * FROM tblreporte WHERE LNGIDREPORTE=$id");
        while ($valores = mysqli_fetch_array($query)) {
    		$this->DetalleReporte[]=$valores; 
    	}
		return $this->DetalleReporte;
	} 
}
?>
