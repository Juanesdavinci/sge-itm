<!DOCTYPE html>
<html>
<head>
	<title>Informe morosos</title>
	<script src="estilos/assets/js/vendor/jquery.min.js"></script>
</head>
<body>
	<button id="limpiar">Limpiar</button>
	<form action="informeCTRL.php" method="post">
		<table>
			<tr>
				<td>
					<label>Ordenado por facultad</label>
				</td>
				<td>
					<input type="text" name="Informe[facultad]" id="facultad" readonly="readonly">
				</td>
				<td>
					<a href="#" class="marcar" btn="facultad"><img src="../img/check.png"></a>
				</td>
			</tr>
			<tr>
				<td>
					<label>Ordenado por programa</label>
				</td>
				<td>
					<input type="text" name="Informe[programa]" id="programa" readonly="readonly">
				</td>
				<td>
					<a href="#" class="marcar" btn="programa"><img src="../img/check.png"></a>
				</td>
			</tr>
			<tr>
				<td>
					<label>Ordenado por categoria</label>
				</td>
				<td>
					<input type="text" name="Informe[categoria]" id="categoria" readonly="readonly">
				</td>
				<td>
					<a href="#" class="marcar" btn="categoria"><img src="../img/check.png"></a>
				</td>
			</tr>
			<tr>
				<td>
					<label>Ordenado por dias de atraso</label>
				</td>
				<td>
					<input type="text" name="Informe[dias]" id="dias" readonly="readonly">
				</td>
				<td>
					<a href="#" class="marcar" btn="dias"><img src="../img/check.png"></a>
				</td>
			</tr>
			<tr>
				<td>
					<label>Ordenado por Entregado / no Entregado</label>
				</td>
				<td>
					<input type="text" name="Informe[entregado]" id="entregado" readonly="readonly">
				</td>
				<td>
					<a href="#" class="marcar" btn="entregado"><img src="../img/check.png"></a>
				</td>
			</tr>
		</table>
		<input type="hidden" id="orden" name="orden" value="0">
		<input type="submit" name="Enviar">
	</form>
</body>
</html>


<script type="text/javascript">
	$(".marcar").click(function(){
		var id_marcar = $(this).attr('btn');
		var orden = $("#orden").val();
		orden++;
		$("#"+id_marcar).val(orden);
		$("#orden").val(orden);
	})

	$("#limpiar").click(function(){
		$("form input:text").val('');
		$("#orden").val(0);
	})
</script>
<?php


