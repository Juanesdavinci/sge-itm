<?php
include ('../ENT/EmpleadoENT.php');

class EmpleadoCTR
{
	private $arrEmpleado;
	public function __construct()
	{  
	    $this->arrEmpleado = array();
	}
	public function CrearEmpleado($user_id, $name, $address, $email, $cellphone, $phone, $start_date, $job)
	{
		$Usuario = new EmpleadoENT();
		$this->arrEmpleado = $Usuario->CrearEmpleado($user_id, $name, $address, $email, $cellphone, $phone, $start_date, $job);
		return $this->arrEmpleado;
	} 
}
?>
